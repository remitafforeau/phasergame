function allPropToFalse(obj){ for(let prop in obj){ obj[prop] = false; } }

function animPlayerInHisDirection(){
    let anim       = 'none';
    let variation  = '';

    switch(this.player.overlapTileType){
        case 'feetHiding':
            variation = 'WithoutFeet';
        break;
        case 'puddleWater':
            variation = 'WithDropsOnFeets';
        break;
    }

    let moveType   = !this.cursors.shift.isDown ? `player${variation}WalkTo` : `player${variation}RunTo`;
    let isShooting = !this.player.isShooting ? '' : 'Shooting';

    switch(this.player.direction){
        case 'up':
            anim = moveType + 'Up' + isShooting;
        break;
        case 'right':
            anim = moveType + 'Right' + isShooting;
        break;
        case 'down':
            anim = moveType + 'Down' + isShooting;
        break;
        case 'left':
            anim = moveType + 'Left' + isShooting;
        break;
        case 'upAndRight':
            anim = moveType + 'UpRight' + isShooting;
        break;
        case 'upAndLeft':
            anim = moveType + 'UpLeft' + isShooting;
        break;
        case 'downAndRight':
            anim = moveType + 'DownRight' + isShooting;
        break;
        case 'downAndLeft':
            anim = moveType + 'DownLeft' + isShooting;
        break;
    }
    if(this.player.direction !== 'none'){
        if(!this.player.anims.animationManager.get(anim) || !this.player.anims.animationManager.get(anim).paused){ this.player.play(anim, true); }
        else{ this.player.anims.animationManager.get(anim).resume(); }
    }
    else{
        if(this.player.anims.animationManager.get(anim)){ this.player.anims.animationManager.get(anim).pause(); }
        else{ this.player.anims.stop(); }
    }
}

function bulletDirection(){
    return this.strDirToObjDir(this.player.direction !== 'none' ? this.player.direction : this.player.orientTo);
}
function bulletEnemyDirection(enemy){
    return this.strDirToObjDir(enemy.direction);
}

function createMap(mapConfigObj){
    initMapInfos(mapConfigObj);

    this.map     = this.make.tilemap({ key: mapConfigObj.key });
    this.tileset = this.map.addTilesetImage(mapConfigObj.tileset.fileName, mapConfigObj.tileset.key);

    mapConfigObj.layersObjs             = [];
    mapConfigObj.layersWithCollides     = [];
    mapConfigObj.layersObjsWithCollides = [];
    mapConfigObj.layersWithOverlaps     = [];
    mapConfigObj.layersObjsWithOverlaps = [];
    mapConfigObj.ObjectsGroupsToCollide = [];
    mapConfigObj.ObjectsGroupsToOverlap = [];

    mapConfigObj.layers.forEach(layerObj => {
        this[layerObj.varName] = layerObj.type && layerObj.type === 'object'  ? this.map.getObjectLayer(layerObj.name) : this.map.createLayer(layerObj.name, this.tileset);

        mapConfigObj.layersObjs.push(this[layerObj.varName]);

        if(layerObj.depth){ this[layerObj.varName].setDepth(layerObj.depth); }

        if(layerObj.collide){
            if(layerObj.type !== 'object'){
                this[layerObj.varName].setCollisionByProperty({ collides: true });
                mapConfigObj.layersWithCollides.push(layerObj);
                mapConfigObj.layersObjsWithCollides.push(this[layerObj.varName]);
            }
            else{
                const objectsFromLayer = this.map.createFromObjects(layerObj.name);

                this[layerObj.name + 'ObjectGroup'] = this.physics.add.group();

                objectsFromLayer.forEach(objectFromLayer => {
                    this.physics.world.enable(objectFromLayer);
                    objectFromLayer.body.setAllowGravity(false);

                    this[layerObj.name + 'ObjectGroup'].add(objectFromLayer);
                });

                this[layerObj.name + 'ObjectGroup'].children.each(object => {
                    object.body.setAllowGravity(false);
                    object.setOrigin(0.5, -0.5);
                    object.body.setImmovable(true);
                    object.setVisible(false);   
                });

                this[layerObj.name + 'ObjectGroup'].groupsWithNoCollide = layerObj.groupsWithNoCollide ? layerObj.groupsWithNoCollide : [];

                mapConfigObj.ObjectsGroupsToCollide.push(this[layerObj.name + 'ObjectGroup']);
            }
        }

        if(layerObj.overlap){
            if(layerObj.type !== 'object'){
                mapConfigObj.layersWithOverlaps.push(layerObj);
                mapConfigObj.layersObjsWithOverlaps.push(this[layerObj.varName]);
            }
            else{
                const objectsFromLayer = this.map.createFromObjects(layerObj.name);

                this[layerObj.name + 'ObjectGroup'] = this.physics.add.group();

                objectsFromLayer.forEach(objectFromLayer => {
                    this.physics.world.enable(objectFromLayer);

                    this[layerObj.name + 'ObjectGroup'].add(objectFromLayer);
                });

                this[layerObj.name + 'ObjectGroup'].children.each(object => {
                    object.body.setAllowGravity(false);
                    object.setOrigin(0.5, -0.5);
                    object.body.setImmovable(true);
                    object.setVisible(false);   
                });

                mapConfigObj.ObjectsGroupsToOverlap.push(this[layerObj.name + 'ObjectGroup']);
            }
        }
    });

    if(mapConfigObj.isAnimatedTiles){
        this.sys.animatedTiles.init(this.map);
        this.sys.animatedTiles.updateAnimatedTiles();
    }
    
    this.forEachLayer(layer => {
        if(layer.type === 'TilemapLayer'){
            layer.forEachTile(tile => {
                tile.inexInTiled = tile.index - 1;
            });
        }
    });
}

function debugTileBelowPointerClick(mapConfigObj){
    let platform = this.platform;
    
    if (this.showDebug && this.input.manager.activePointer.isDown && !this.previousPointerDown){
        const pointer = this.input.manager.activePointer;

        if(!this.debugTiles){ this.debugTiles = []; this.allDebugedTiles = []; }

        this.allDebugedTiles.forEach(tile => { tile.tint = TILE_ORIGIN_TINT; });

        mapConfigObj.layersObjs.forEach((layer, i) => {
            if(layer.type === 'TilemapLayer'){
                this.debugTiles[i] = { layer: layer.layer.name, tile: this.map.getTileAtWorldXY(pointer.worldX, pointer.worldY, false, this.cameras.main, layer) };
                if(this.debugTiles[i].tile){
                    this.debugTiles[i].tile.tint     = TILE_SELECTED_TINT;
                    this.debugTiles[i].tile.selected = true;

                    this.allDebugedTiles.push(this.debugTiles[i].tile);
                }
            }
        });
        this.showDebugTilesInfos();
    }
    this.previousPointerDown = this.input.manager.activePointer.isDown;
}

function drawDebug (mapConfigObj, layersObjs){
    if(!this.debugGraphics){ this.debugGraphics = this.add.graphics(); }
    this.debugGraphics.clear();

    if (this.showDebug){
        mapConfigObj.layersObjsWithCollides.forEach(layer => {
            // Pass in null for any of the style options to disable drawing that component
            layer.renderDebug(this.debugGraphics, {
                tileColor: null, // Non-colliding tiles
                collidingTileColor: new Phaser.Display.Color(243, 134, 48, 150), // Colliding tiles
                faceColor: new Phaser.Display.Color(0, 0, 0, 255) // Colliding face edges
            });
        });
        this.setMapTilesSize(this.map.tileWidth - 1, this.map.tileHeight - 1, layersObjs);
    }
    else{
        this.setMapTilesSize(this.map.tileWidth, this.map.tileHeight, layersObjs);
    }
}

function fireBullet(direction){
    let bullet       = this.physics.add.sprite(this.player.x, this.player.y, gameParams.player.bullet.sprite).setBounce(1).setScale(0.79).refreshBody();
    bullet.direction = direction;

    if(gameParams.player.bullet.radius){ bullet.setCircle(gameParams.player.bullet.radius); }

    bullet.setDepth(1);

    bullet.speed = gameParams.player.bullet.velocity;

    bullet.body.setVelocityX(direction.x);
    bullet.body.setVelocityY(direction.y);

    this.bulletGroup.add(bullet);
}

function fireCircleBullets(nb, speedCoeff = 1){
    const step = PI2/nb;
    for(let i = 0; i < PI2 ; i+=step){
        this.fireBullet(direction(i, speedCoeff));
    }
}

function fireBulletEnemy(enemy){
    let direction         = this.bulletEnemyDirection(enemy);
    let bulletEnemy       = this.physics.add.sprite(enemy.x, enemy.y, enemy.typeOptions.bullet.sprite).setBounce(1).setScale(0.79).refreshBody();
    bulletEnemy.direction = direction;
    bulletEnemy.typeEnemy = enemy.typeEnemy;

    if(enemy.typeOptions.bullet.radius){ bulletEnemy.setCircle(enemy.typeOptions.bullet.radius); }

    bulletEnemy.setDepth(1);

    bulletEnemy.speed = enemy.typeOptions.bullet.velocity;

    bulletEnemy.setRotation(direction.x ?  HALF_PI*Math.sign(direction.x) : (direction.y > 0 ? PI : 0));
    if(direction.x){
        bulletEnemy.body.setSize(bulletEnemy.body.height, bulletEnemy.body.width);
        bulletEnemy.refreshBody();
        bulletEnemy.setCircle(enemy.typeOptions.bullet.radius);
    }

    bulletEnemy.body.setVelocityX(direction.x);
    bulletEnemy.body.setVelocityY(direction.y);

    this.bulletEnemiesGroup.add(bulletEnemy);
}

function initMapInfos(mapConfigObj){
    let tileset  = mapConfigObj.tileset;
    let filePath = tileset.filePath;
    let sepIndex = filePath.lastIndexOf('/') + 1;

    tileset.fileName = tileset.filePath.substr(sepIndex, tileset.filePath.length - (sepIndex + 4));

    mapConfigObj.layers = mapConfigObj.layers.map(layer => {
        layer.varName = 'layer' + layer.name.replace(/[-,*,$]/g, '');
        return layer;
    });
    mapConfigObj.aboveLayer = mapConfigObj.layers[mapConfigObj.layers.length - 1];
}

function isAllPropToFalse(obj){
    for(let prop in obj){
        if(obj[prop]){ return false; }
    }
    return true;
}

function makeUserTuches(){
    this.cursors = this.input.keyboard.createCursorKeys();
    this.keys    = this.input.keyboard.addKeys({
        plus  : Phaser.Input.Keyboard.KeyCodes.NUMPAD_ADD,
        minus : Phaser.Input.Keyboard.KeyCodes.NUMPAD_SUBTRACT,
        nine  : Phaser.Input.Keyboard.KeyCodes.NUMPAD_NINE,
        six   : Phaser.Input.Keyboard.KeyCodes.NUMPAD_SIX,
        four  : Phaser.Input.Keyboard.KeyCodes.NUMPAD_FOUR,
    });

    this.spaceKey = this.input.keyboard.addKey('SPACE', true, false);
    this.pauseKey = this.input.keyboard.addKey('P', true, false);
}

function moveBullets(){
    this.bulletGroup.children.each(function(bullet) {
        if(bullet.direction){
            bullet.body.setVelocityX(bullet.direction.x * bullet.speed);
            bullet.body.setVelocityY(bullet.direction.y * bullet.speed);
        }
        if(bullet.x < 0 || bullet.x > areneSizes.map.width || bullet.y < 0 || bullet.y > areneSizes.map.height){
            bullet.destroy();
        }
    }, this);
}
function moveBulletsEnemies(){
    this.bulletEnemiesGroup.children.each(function(bulletEnemy) {
        if(bulletEnemy.direction){
            bulletEnemy.body.setVelocityX(bulletEnemy.direction.x * bulletEnemy.speed);
            bulletEnemy.body.setVelocityY(bulletEnemy.direction.y * bulletEnemy.speed);
        }
        if(bulletEnemy.x < 0 || bulletEnemy.x > areneSizes.map.width || bulletEnemy.y < 0 || bulletEnemy.y > areneSizes.map.height){
            bulletEnemy.destroy();
        }
    }, this);
}

function playerDirection(){
    if(this.cursors.left.isDown){
        if(!this.arrowKeys.up && !this.arrowKeys.down){ return 'left'; }
        if(this.arrowKeys.up){   return 'upAndLeft'; }
        if(this.arrowKeys.down){ return 'downAndLeft'; }
    }

    if(this.cursors.right.isDown){
        if(!this.arrowKeys.up && !this.arrowKeys.down){ return 'right'; }
        if(this.arrowKeys.up){   return 'upAndRight'; }
        if(this.arrowKeys.down){ return 'downAndRight'; }
    }

    if(this.cursors.up.isDown){
        if(!this.arrowKeys.left && !this.arrowKeys.right){ return 'up'; }
        if(this.arrowKeys.left){  return 'upAndLeft'; }
        if(this.arrowKeys.right){ this.player.direction  = 'upAndRight'; }
    }

    if(this.cursors.down.isDown){
        if(!this.arrowKeys.left && !this.arrowKeys.right){ return 'down'; }
        if(this.arrowKeys.left){  return 'downAndLeft'; }
        if(this.arrowKeys.right){ this.player.direction  = 'downAndRight'; }
    }
    return 'none';
}

function setMapTilesSize(tileSizeX, tileSizeY, layersObjs){
    layersObjs.forEach(layer => {
        this.map.setLayerTileSize(tileSizeX, tileSizeY, layer);
    }); 
}

function showDebugTilesInfos(){
    if(this.debugTiles.length){
        console.log("************DEBUGGING TILES BELOW POIONTER**************");
        this.debugTiles.forEach(tile => {
            console.log(" ");
            console.log("Layer : ", tile.layer);
            console.log("Tile  : ", tile.tile);
        });
        console.log("////////////////////////////////////////////////////");
        console.log(" ");
        console.log(" ");
    }
}

function  showHealthBar (sprite, options){
    let pos = options.pos;
    let x   = pos.x;
    let y   = pos.y;

    let sizes  = options.sizes;
    let width  = sizes.width;
    let height = sizes.height;

    let borderThickness   = options.border.thickness;
    let borderColor       = options.colors.border;
    let bgNoHealth        = options.colors.bg.noHealth;
    let lifeInHealthColor = options.colors.life.inHealth;
    let lifeInDangerColor = options.colors.life.inDanger;
    let lifeInDangerLimit = options.lifeInDangerLimit;
    
    let spriteLife      = sprite.life;
    let spriteStarLife  = sprite.startLife;

    let cameraMidPoint = this.cameras.main.midPoint;

    let healthBar = sprite.healthBar ? sprite.healthBar : this.add.graphics({ x: x, y: y });


    x += cameraMidPoint.x - areneSizes.center.x;
    y += cameraMidPoint.y - areneSizes.center.y;


    healthBar.clear();

    //  BG
    healthBar.fillStyle(borderColor);
    healthBar.fillRect(x, y, width, height);

    //  Health
    healthBar.fillStyle(bgNoHealth);
    healthBar.fillRect(x + borderThickness, y + borderThickness, width - (2*borderThickness), height - (2*borderThickness));
    healthBar.fillStyle(spriteLife/spriteStarLife > lifeInDangerLimit ? lifeInHealthColor : lifeInDangerColor);
    if(spriteLife){ healthBar.fillRect(x + borderThickness, y + borderThickness, (spriteLife * width/spriteStarLife) - (2*borderThickness), height - (2*borderThickness)); }

    sprite.healthBar = healthBar;
}

function strDirToObjDir(strDir){
    switch(strDir){
        case 'up'           : return {x:  0,  y: -1}; 
        case 'right'        : return {x:  1,  y:  0}; 
        case 'down'         : return {x:  0,  y:  1}; 
        case 'left'         : return {x: -1,  y:  0};
        case 'upAndLeft'    : return {x: -1,  y: -1};
        case 'upAndRight'   : return {x:  1,  y: -1};
        case 'downAndLeft'  : return {x: -1,  y:  1};
        case 'downAndRight' : return {x:  1,  y:  1};
    }
    return {x:  0,  y:  0};
}

function varyPlayerLife(variation){
    let newPlayerLifeVal = this.player.life + variation;

    variation < 0 ? (newPlayerLifeVal > 0 ? this.player.life+=variation : this.player.life = 0):
                    (newPlayerLifeVal < this.player.startLife ? this.player.life+=variation : this.player.life = this.player.startLife);
}