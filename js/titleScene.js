/* global Phaser */

class TitleScene extends Phaser.Scene {
    constructor(){
        super({ key: 'titleScene' });

        this.titleSceneBackgroundImage = null;
        this.titleSceneText            = null;
        this.titleSceneTextStyle       = {
            font  : '200px Times',
            fill  : '#fde4b9',
            align : 'center',
        };
    }

    init(data){
        this.cameras.main.setBackgroundColor('#ffffff');
    }

    preload(){
        console.log("Title Scene");
        this.load.image('titleSceneBackground', 'assets/aliens_screen_image.jpg');
    }

    create(data){
        this.titleSceneBackgroundImage   = this.add.sprite(0, 0, 'titleSceneBackground').setScale(2.75);
        this.titleSceneBackgroundImage.x = areneSizes.center.x;
        this.titleSceneBackgroundImage.y = areneSizes.center.y;

        this.titleSceneText = this.add.text(areneSizes.center.x, areneSizes.center.y + 150, "Space Aliens", this.titleSceneTextStyle).setOrigin(0.5);

        this.input.once('pointerdown', function () {
            this.scene.switch('menuScene');
        }, this);
    }

    update(time, delta){
        this.scene.switch('menuScene');
    }
}

export default TitleScene;