/* global Phaser */

class PlatformScene extends Phaser.Scene {
    constructor(){
        super({
            key: 'platformScene',
            physics: {
                arcade: {
                    debug: true,
                    gravity: { x: 0, y: 300 },
                },
            },
        });

        this.sounds    = {};
        this.arrowKeys = { up: false, right: false, down: false, up: false, space: false };
    }

    init(data){
        this.cameras.main.setName('main');

        this.runMod            = 'game';
        this.platform          = gameParams.map[this.runMod].platformScenes[data.platform];
        this.platformVarName   = data.platform;
        this.dataGameScene     = data.dataGameScene;
    }

    preload(){
        let platform = this.platform;
        console.log("Platform Scene");

        // plugins
        this.load.scenePlugin('AnimatedTiles', 'js/animatedTiles.js', 'animatedTiles', 'animatedTiles');

        // sounds
        this.load.audio('playerHurt', 'assets/playerHurt.wav');
        this.load.audio('enemyDie', 'assets/enemyDie.wav');

        // map
        this.load.image(platform.tileset.key, platform.tileset.filePath);
        this.load.tilemapTiledJSON(platform.key, platform.filePath);
    }

    create(data){
        this.globalEvents();
        this.createMap(this.platform);
        this.makeUserTuches();
        this.createCharacters();
        this.setCamera();
        this.addColliders();

        gameParams.gameScene = this;
        zeScene              = this;

        this.physics.world.drawDebug = false;
    }

    update(time, delta){
        this.showHealthBar(this.player, gameParams.player.healthBar);
        this.moveEnemies();
        this.moveBulletsEnemies();
        this.movePlayer();
        this.moveBullets();
        this.debugTileBelowPointerClick(this.platform);
    }

    //***************************************** SECONDARIES FUNCTIONS *****************************************//
    addColliders(){
        let platform   = this.platform;
        this.colliders = {};

        this.bulletGroup        = this.physics.add.group();
        this.bulletEnemiesGroup = this.physics.add.group();

        platform.layersWithCollides.forEach(layerObj => {
            const layerName = layerObj.varName;
            let layer = this[layerName];

            this.colliders[layerName] = {};
            this.colliders[layerName].betweenPlayerAndLayer  = this.physics.add.collider(this.player, layer, function(playerCollide, tileCollide){}.bind(this));
            this.colliders[layerName].betweenEnemiesAndLayer = this.physics.add.collider(this.enemyGroup, layer, function(enemyCollide, tileCollide){}.bind(this));

            if(layerObj.autoCollide){ this.physics.add.collider(layer, layer); }

            if(layerObj.collideWithOthersLayers){
                layerObj.collideWithOthersLayers.forEach(theLayerName => {
                    this.physics.add.collider(this['layer' + theLayerName], layer);
                });
            }
            if(layerObj.collideWithOthersObjectLayers){
                layerObj.collideWithOthersObjectLayers.forEach(theLayerName => {
                    this.physics.add.collider(this[theLayerName + "ObjectGroup"], layer, function(){
                        console.log('ok');
                    });
                });
            }
        });
        
        this.map.setCollisionByExclusion([ -1, 0 ]);
        this.physics.world.setBounds(0, 0, this.map.widthInPixels, this.map.heightInPixels, true, true, true, true);
        this.player.setCollideWorldBounds(true);

        platform.ObjectsGroupsToCollide.forEach(ObjectGroupToCollide => {
            this.physics.add.collider(this.enemyGroup, ObjectGroupToCollide, function(enemyCollide, objectCollide){
                enemyCollide.direction = enemyCollide.direction === 'right' ? 'left' : 'right';
            });
        });

        platform.ObjectsGroupsToOverlap.forEach(ObjectGroupToOverlap => {
            this.physics.add.overlap(this.player, ObjectGroupToOverlap, function(playerOverlap, objectOverlap){

                playerOverlap.overlappingObjects.add(objectOverlap);
            });
        });

        this.physics.add.collider(this.player, this.enemyGroup, function(playerCollide, enemyCollide) {
            if(playerCollide.direction === enemyCollide.direction){
                enemyCollide.destroy();
                if (!this.sounds.enemyDie.isPlaying){
                    this.sounds.enemyDie.play();
                    this.varyPlayerLife(gameParams.enemy.type[enemyCollide.typeEnemy].power);
                }
            }
            else{
                this.varyPlayerLife(-gameParams.enemy.type[enemyCollide.typeEnemy].power/10);
                if (!this.sounds.playerHurt.isPlaying){ this.sounds.playerHurt.play(); }
            }
        }.bind(this));

        this.physics.add.collider(this.player, this.bulletEnemiesGroup, function(playerCollide, bulletCollide) {
            this.varyPlayerLife(-gameParams.enemy.type[bulletCollide.typeEnemy].power);
            bulletCollide.destroy();
        }.bind(this));

        this.physics.add.collider(this.enemyGroup, this.bulletGroup, function(enemyCollide, bulletCollide) {
            enemyCollide.destroy();
            bulletCollide.destroy();
        }.bind(this));
    }

    allPropToFalse           = allPropToFalse;
    animPlayerInHisDirection = animPlayerInHisDirection;
    bulletDirection          = bulletDirection;
    bulletEnemyDirection     = bulletEnemyDirection;

    createCharacters(){
        this.sounds.playerHurt = this.sound.add('playerHurt');  
        this.sounds.enemyDie   = this.sound.add('enemyDie');

        this.createPlayer();
        this.createEnemies();
    }

    createEnemies(){
        this.enemyGroup = this.physics.add.group({ collideWorldBounds: true });

        if(!this.platform.enemyGroupSave){
            this.ObjectCollisionObjectGroup.children.each((objectCollision, i) => {
                if(objectCollision.data && objectCollision.data.list.startPosEnemy){
                    let enemyInfos = {value: {platform: false}};

                    while(!enemyInfos.value.platform){
                        enemyInfos = getRandomProperty(gameParams.enemy.type);
                    }
            
                    const enemyType  = enemyInfos.prop; 
                    let enemyOptions = enemyInfos.value;

                    let pos   = objectCollision.getRightCenter();
                    let enemy = this.physics.add.sprite(pos.x + objectCollision.displayWidth * 1 + 10, pos.y).play({ key: 'enemiesStart' + enemyType, repeat: -1 }).setImmovable().setScale(1.72).refreshBody().setInteractive();

                    enemy.body.position.x = enemy.x;
                    enemy.body.position.y = enemy.y;

                    enemy.body.setSize(enemy.width * enemyOptions.sizeCoeff.x, enemy.height * enemyOptions.sizeCoeff.y);
                    enemy.body.setOffset(enemy.width * enemyOptions.offsetCoeff.x, enemy.height * enemyOptions.offsetCoeff.y);

                    enemy.typeEnemy   = enemyType;
                    enemy.typeOptions = enemyOptions;

                    enemy.name = "enemy_" + enemyType + "_" + i;

                    enemy.on('pointerdown', function (pointer)
                    {
                        enemy.talkingTo       = !enemy.talkingTo;
                        this.player.talkingTo = enemy.talkingTo;

                        this.enemyGroup.children.each(function(enemyInGroup) {
                            if(enemyInGroup.name !== enemy.name){ enemyInGroup.talkingTo = false; }
                        }, this);

                    }, this);

                    this.enemyGroup.add(enemy);
                }
            });
        }
        else{
            this.platform.enemyGroupSave.forEach(enemySave => {
                const enemyType    = enemySave.typeEnemy;
                const enemyOptions = enemySave.typeOptions;
                
                let enemy = this.physics.add.sprite(enemySave.x, enemySave.y).play({ key: 'enemiesStart' + enemyType, repeat: -1 }).setImmovable().setScale(1.72).refreshBody().setInteractive();

                enemy.body.position.x = enemy.x;
                enemy.body.position.y = enemy.y;

                enemy.body.setSize(enemy.width * enemyOptions.sizeCoeff.x, enemy.height * enemyOptions.sizeCoeff.y);
                enemy.body.setOffset(enemy.width * enemyOptions.offsetCoeff.x, enemy.height * enemyOptions.offsetCoeff.y);

                enemy.typeEnemy   = enemyType;
                enemy.typeOptions = enemyOptions;

                enemy.name = enemySave.name;

                enemy.on('pointerdown', function (pointer)
                {
                    enemy.talkingTo       = !enemy.talkingTo;
                    this.player.talkingTo = enemy.talkingTo;

                    this.enemyGroup.children.each(function(enemyInGroup) {
                        if(enemyInGroup.name !== enemy.name){ enemyInGroup.talkingTo = false; }
                    }, this);

                }, this);

                this.enemyGroup.add(enemy);
            });
        }
        
        this.enemyGroup.setDepth(1);
    }

    createMap = createMap;

    createPlayer(){
        let platform = this.platform;

        this.player = this.physics.add.sprite(platform.player.startPos.x, platform.player.startPos.y).play({ key: 'playerStart', repeat: -1 }).setDrag(222).setScale(0.6).refreshBody();

        this.player.body.setSize(this.player.width/3, this.player.height/Math.SQRT2);
        this.player.body.setOffset(this.player.width * gameParams.offsetCoeff.x, this.player.height * gameParams.offsetCoeff.y);

        this.player.orientTo  = 'down';
        this.player.direction = 'none';
        this.player.nbBurns   = 0;
        this.player.startLife = 100;
        this.player.life      = this.dataGameScene.player.life;

        this.player.overlappingObjects = new Set();

        this.player.setDepth(1);
    }

    debugTileBelowPointerClick = debugTileBelowPointerClick;
    drawDebug                  = drawDebug;
    fireBullet                 = fireBullet;
    fireBulletEnemy            = fireBulletEnemy;

    forEachLayer(func, ...args){
        this.platform.layersObjs.forEach(layer => {
            func(layer, ...args);
        });
    }

    globalEvents(){
        let platform = this.platform;
 
        this.input.keyboard.on('keydown-C', event => {
            this.showDebug               = !this.showDebug;
            this.physics.world.drawDebug =  this.showDebug;
            this.enemiesInvDirection     =  this.showDebug;

            this.physics.world.debugGraphic.clear();

            this.forEachLayer(layer => {
                if(layer.currentTilePointerMoveOver){ layer.currentTilePointerMoveOver.tint = TILE_ORIGIN_TINT; }
                if(layer.preventTilePointerMoveOver){ layer.preventTilePointerMoveOver.tint = TILE_ORIGIN_TINT; }
            });

            if(this.allDebugedTiles){ this.allDebugedTiles.forEach(tile => { tile.tint = TILE_ORIGIN_TINT; }); }

            this.drawDebug(platform, platform.layersObjs);
        });

        this.input.keyboard.on('keydown-E', event => {
            this.player.overlappingObjects.forEach(overlappingObject => {
                if(overlappingObject.data.list.action === 'out'){
                    this.scene.get('gameScene').data.set('dataPlatformScene', { player : this.player });
                    this.scene.get('gameScene').player.life = this.player.life;

                    if(!this.scene.get('gameScene').dataFromPlatformsScene){
                        this.scene.get('gameScene').dataFromPlatformsScene = {};
                    }
                    if(!this.scene.get('gameScene').dataFromPlatformsScene[this.platformVarName]){
                        this.scene.get('gameScene').dataFromPlatformsScene[this.platformVarName] = {};
                    }

                    this.platform.enemyGroupSave = this.enemyGroup.children.entries.map(enemy => {
                        return {
                            name        : enemy.name,
                            x           : enemy.x,
                            y           : enemy.y,
                            typeEnemy   : enemy.typeEnemy,
                            typeOptions : enemy.typeOptions,
                        };
                    });

                    this.scene.pause('platformScene');

                    this.scene.setVisible(false, 'platformScene');
                    this.scene.setVisible(true, 'gameScene');
                    this.scene.resume('gameScene');
                }
            });
        });

        this.input.keyboard.on('keydown-M', event => {
            this.cameras.main.zoom+=0.1;
        });
        this.input.keyboard.on('keydown-O', event => {
            this.cameras.main.zoom = 1;
        });
        this.input.keyboard.on('keydown-L', event => {
            this.cameras.main.zoom-=0.1;
        });

        this.input.on('pointermove', (pointer) => {
            if(this.showDebug){
                this.forEachLayer(layer => {
                    layer.currentTilePointerMoveOver = this.map.getTileAtWorldXY(pointer.worldX, pointer.worldY, false, this.cameras.main, layer);
                    if(layer.currentTilePointerMoveOver){
                        if(layer.preventTilePointerMoveOver && layer.preventTilePointerMoveOver !== layer.currentTilePointerMoveOver){
                            if(!layer.preventTilePointerMoveOver.selected){ layer.preventTilePointerMoveOver.tint = TILE_ORIGIN_TINT; }
                        }
        
                        if(!layer.currentTilePointerMoveOver.selected){ layer.currentTilePointerMoveOver.tint = TILE_MOUSE_OVER_TINT; }
                        layer.preventTilePointerMoveOver      = layer.currentTilePointerMoveOver;
                    }
                });
            }
        });
    }

    initMapInfos(){
        let platform = this.platform;
        let tileset  = platform.tileset;
        let filePath = tileset.filePath;
        let sepIndex = filePath.lastIndexOf('/') + 1;
    
        tileset.fileName = tileset.filePath.substr(sepIndex, tileset.filePath.length - (sepIndex + 4));
    
        platform.layers = platform.layers.map(layer => {
            layer.varName = 'layer' + layer.name.replace(/[-,*,$]/g, '');
            return layer;
        });
        platform.aboveLayer = platform.layers[platform.layers.length - 1];
    }

    isAllPropToFalse   = isAllPropToFalse;
    makeUserTuches     = makeUserTuches;
    moveBullets        = moveBullets;
    moveBulletsEnemies = moveBulletsEnemies;

    moveEnemies(){
        this.enemyGroup.children.each(function( enemy ) {
            if(!enemy.notFirstMove){
                enemy.direction = 'right';
                enemy.nbLaps    = 0;
            }

            switch(enemy.direction ){
                case 'right':
                    enemy.body.setVelocityX(enemy.typeOptions.velocity);
                    enemy.body.setVelocityY(0);
                    enemy.play('enemies' + enemy.typeEnemy + 'WalkToRight', true);
                break;
                case 'left':
                    enemy.body.setVelocityX(-enemy.typeOptions.velocity);
                    enemy.body.setVelocityY(0);
                    enemy.play('enemies' + enemy.typeEnemy + 'WalkToLeft', true);
                break;
            }

            if(enemy.nbLaps%enemy.typeOptions.bullet.frameRate==0 && Phaser.Math.Distance.Between(this.player.x, this.player.y, enemy.x, enemy.y) < enemy.typeOptions.distToShoot / 2){
                this.fireBulletEnemy(enemy);
            }

            enemy.notFirstMove = true;
            enemy.nbLaps++;
          }, this);
    }

    movePlayer(){
        if(this.player.direction !== 'none'){ this.player.orientTo = this.player.direction; }
        this.player.direction  = 'none';
        this.player.isShooting = false;

        this.allPropToFalse(this.arrowKeys);
        if(this.player.overlapTileType !== 'sliding'){ this.player.setVelocityX(0); }

        let velocity = !this.cursors.shift.isDown ? gameParams.player.velocity : 2 * gameParams.player.velocity;

        if(this.cursors.left.isDown){
            this.arrowKeys.left = true;

            this.player.body.setVelocityX(-velocity);
        }
        else if(this.cursors.right.isDown === true){
            this.arrowKeys.right = true;

            this.player.body.setVelocityX(velocity);
        }
        if(this.cursors.up.isDown === true && this.player.body.onFloor()){
            this.arrowKeys.up = true;

            this.player.body.setVelocityY(-velocity);
        }

        this.player.direction = this.playerDirection();

        this.player.direction = this.player.direction === 'up' || this.player.direction === 'down' ? 'none' : this.player.direction;

        if(Phaser.Input.Keyboard.JustDown(this.spaceKey)){
            this.player.isShooting = true;

            this.fireBullet(this.bulletDirection());
        }

        this.animPlayerInHisDirection();
    }

    playerDirection = playerDirection;

    setCamera(){
        this.cameras.main.setBounds(0, 0, areneSizes.platform.width, areneSizes.platform.height);
        this.cameras.main.setZoom(1);
        this.cameras.main.centerOn(0, 0);
        this.cameras.main.startFollow(this.player, true, 0.5, 0.5);
    }

    setMapTilesSize     = setMapTilesSize;
    showDebugTilesInfos = showDebugTilesInfos;
    showHealthBar       = showHealthBar;
    strDirToObjDir      = strDirToObjDir;
    varyPlayerLife      = varyPlayerLife;
}

export default PlatformScene;