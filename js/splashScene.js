/* global Phaser */

class SplashScene extends Phaser.Scene {
    constructor(){
        super({ key: 'splashScene' });
    }

    init(data){
        this.cameras.main.setBackgroundColor('#ffffff');
    }

    preload(){
        console.log("Splash Scene");
        this.load.image('splashSceneBackground', './assets/splashSceneImage.png');
    }

    create(data){
        this.splashSceneBackgroundImage = this.add.sprite(0, 0, 'splashSceneBackground');
        this.splashSceneBackgroundImage.x = areneSizes.center.x;
        this.splashSceneBackgroundImage.y = areneSizes.center.y;

        this.input.once('pointerdown', function () {
            this.scene.switch('titleScene');
        }, this);
    }

    update(time, delta){
        if(time > 1000){ this.scene.switch('titleScene'); }
    }
}

export default SplashScene;