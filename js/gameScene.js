/* global Phaser */

class GameScene extends Phaser.Scene {
    constructor(){
        super({
            key: 'gameScene',
            physics: {
                arcade: {
                    debug: true,
                    gravity: { x: 0, y: 0 },
                },
            },
        });

        this.sounds              = {};
        this.player              = null;
        this.previousPointerDown = false;
        this.enemies             = [];
        this.score               = 0;
        this.scoreText           = null;
        this.scoreTextStyle      = {font: '65px Arial', fill: '#ffffff', align: 'center'};
        this.gameOverText        = null;
        this.gameOverTextStyle   = {font: '65px Arial', fill: '#ffffff', align: 'center'};

        this.arrowKeys = { up: false, right: false, down: false, up: false, space: false };
    }


    init(data){
        this.runMod = data.mod;
        this.cameras.main.setName('main');
        
        let debugCam = this.cameras.add(0, 0, 0, 0).setVisible(false);

        debugCam.setName('debugCam');
        debugCam.setBounds(0, 0, areneSizes.map.width, areneSizes.map.height);
        debugCam.setZoom(0.1, 0.1);
        debugCam.centerOn(0, 0);
        debugCam.setSize(gameParams.cameras.debugCam.size.x, gameParams.cameras.debugCam.size.y);
        debugCam.setPosition(0, 0);
        debugCam.setRoundPixels(true);
    }

    preload(){
        console.log("Game Scene");

        // plugins
        this.load.scenePlugin('AnimatedTiles', 'js/animatedTiles.js', 'animatedTiles', 'animatedTiles');

        // images
        this.load.image('bullet', 'assets/bullet.png');

        for(let cloud in gameParams.decor.clouds){
            this.load.image(cloud, `assets/${cloud}.png`);
        }

        for(let enemyType in gameParams.enemy.type){
            let spriteName = gameParams.enemy.type[enemyType].bullet.sprite;
            this.load.image(spriteName, `assets/${ spriteName }.png`);
        }

        // sounds
        this.load.audio('playerHurt', 'assets/playerHurt.wav');
        this.load.audio('enemyDie', 'assets/enemyDie.wav');

        // map
        this.load.image(gameParams.map[this.runMod].tileset.key, gameParams.map.game.tileset.filePath);
        this.load.tilemapTiledJSON(gameParams.map[this.runMod].key, gameParams.map[this.runMod].filePath);
        
        // sprite sheet
        this.load.spritesheet('linkWalking', 'assets/playerSprites.png', { frameWidth: 100, frameHeight: 100 });
        this.load.spritesheet('linkWithoutFeetWalking', 'assets/playerWithoutFeetSprites.png', { frameWidth: 100, frameHeight: 100 });
        this.load.spritesheet('linkWithDropsOnFeetsWalking', 'assets/playerSpritesWithDrops.png', { frameWidth: 100, frameHeight: 100 });
        this.load.spritesheet('enemiesWalking', 'assets/enemiesSpritesSheet.png', { frameWidth: 29, frameHeight: 29 });
        this.load.spritesheet('fishSwimming', 'assets/fishSwimming.png', { frameWidth: 32, frameHeight: 32 });

        //DOM Elem
        this.load.html('domInput', 'domElem/inputText.html');
    }

    create(data){
        this.globalEvents();
        this.createMap(gameParams.map[this.runMod]);
        this.createAnims();
        this.makeUserTuches();
        this.createCharacters();
        this.createDecor();
        this.setCamera();
        this.addColliders();

        //To fix input.on problem
        document.getElementById('gameContainer').firstChild.style.zIndex = -10;

        gameParams.gameScene = this;
        zeScene              = this;

        this.physics.world.drawDebug = false;
    }

    update(time, delta){
        this.showHealthBar(this.player, gameParams.player.healthBar);
        this.moveEnemies();
        this.moveBulletsEnemies();
        this.movePlayer();
        this.moveFishes();
        this.moveBullets();
        this.moveDecor();
        this.showBubbles();
        this.debugTileBelowPointerClick(gameParams.map[this.runMod]);
    }

    //***************************************** SECONDARIES FUNCTIONS *****************************************//
    addColliders(){
        this.colliders = {};

        this.bulletGroup        = this.physics.add.group();
        this.bulletEnemiesGroup = this.physics.add.group();

        gameParams.map[this.runMod].ObjectsGroupsToCollide.forEach(ObjectGroupToCollide => {
            this.physics.add.collider(this.player, ObjectGroupToCollide);
            this.physics.add.collider(this.enemyGroup, ObjectGroupToCollide);
            this.physics.add.collider(this.fishGroup, ObjectGroupToCollide);

            if(!ObjectGroupToCollide.groupsWithNoCollide.find(groupWithNoCollide => groupWithNoCollide === 'bulletGroup')){
                this.physics.add.collider(this.bulletGroup, ObjectGroupToCollide, function(bulletCollide, objectCollide){ bulletCollide.destroy(); });
            }
            if(!ObjectGroupToCollide.groupsWithNoCollide.find(groupWithNoCollide => groupWithNoCollide === 'bulletEnemiesGroup')){
                this.physics.add.collider(this.bulletEnemiesGroup, ObjectGroupToCollide, function(bulletEnemyCollide, objectCollide){ bulletEnemyCollide.destroy(); });
            }
        });

        gameParams.map[this.runMod].layersWithCollides.forEach(layerObj => {
            const layerName = layerObj.varName;
            let layer = this[layerName];

            this.colliders[layerName] = {};
            this.colliders[layerName].betweenPlayerAndLayer = this.physics.add.collider(this.player, layer, function(playerCollide, tileCollide){}.bind(this));

            if(layerObj.autoCollide){ this.physics.add.collider(layer, layer); }

            if(layerObj.collideWithOthersLayers){
                layerObj.collideWithOthersLayers.forEach(theLayerName => {
                    this.physics.add.collider(this['layer' + theLayerName], layer);
                });
            }
            if(layerObj.collideWithOthersObjectLayers){
                layerObj.collideWithOthersObjectLayers.forEach(theLayerName => {
                    this.physics.add.collider(this[theLayerName + "ObjectGroup"], layer);
                });
            }

            this.physics.add.collider(this.enemyGroup, layer);
            this.physics.add.collider(this.bulletGroup, layer, function(bulletCollide, tileCollide) {
                bulletCollide.destroy();
            }.bind(this));
            this.physics.add.collider(this.bulletEnemiesGroup, layer, function(bulletCollide, layerCollide) {
                bulletCollide.destroy();
            }.bind(this));
        });
        gameParams.map[this.runMod].layersWithOverlaps.forEach(layerObj => {
            let layer = this[layerObj.varName];

            this.physics.add.overlap(this.player, layer, function(playerOverlap, tileOverlap){
                let overlapTileInfos          = tileOverlap.getTileData();
                playerOverlap.overlapTileType = overlapTileInfos ? overlapTileInfos.type : (tileOverlap.properties.puddleWater ? 'puddleWater' : 'none');
                if(playerOverlap.overlapTileType === 'burning'){ 
                    if(playerOverlap.nbBurns%60==0){
                        this.varyPlayerLife(-10);
                        this.sounds.playerHurt.play();
                    }
                    playerOverlap.nbBurns++;
                 }
             }.bind(this));
            this.physics.add.overlap(this.enemyGroup, layer);
            this.physics.add.overlap(this.bulletGroup, layer);
            this.physics.add.overlap(this.bulletEnemiesGroup, layer);
        });
        
        this.map.setCollisionByExclusion([ -1, 0 ]);
        this.physics.world.setBounds(0, 0, this.map.widthInPixels, this.map.heightInPixels, true, true, true, true);
        this.player.setCollideWorldBounds(true);

        gameParams.map[this.runMod].ObjectsGroupsToOverlap.forEach(ObjectGroupToOverlap => {
            this.physics.add.overlap(this.player, ObjectGroupToOverlap, function(playerOverlap, objectOverlap){
                playerOverlap.overlappingObjects.add(objectOverlap);
            });
        });
        
        this.physics.add.collider(this.player, this.enemyGroup, function(playerCollide, enemyCollide) {
            if(playerCollide.direction === enemyCollide.direction){
                enemyCollide.destroy();
                if (!this.sounds.enemyDie.isPlaying){
                    this.sounds.enemyDie.play();
                    this.varyPlayerLife(gameParams.enemy.type[enemyCollide.typeEnemy].power);
                }
            }
            else{
                this.varyPlayerLife(-gameParams.enemy.type[enemyCollide.typeEnemy].power/10);
                if (!this.sounds.playerHurt.isPlaying){ this.sounds.playerHurt.play(); }
            }
        }.bind(this));

        this.physics.add.collider(this.player, this.bulletEnemiesGroup, function(playerCollide, bulletCollide) {
            this.varyPlayerLife(-gameParams.enemy.type[bulletCollide.typeEnemy].power);
            bulletCollide.destroy();
        }.bind(this));

        this.physics.add.collider(this.enemyGroup, this.bulletGroup, function(enemyCollide, bulletCollide) {
            this.varyPlayerLife(gameParams.enemy.type[enemyCollide.typeEnemy].power);
            enemyCollide.destroy();
            bulletCollide.destroy();
        }.bind(this));

        this.physics.add.collider(this.bulletGroup, this.bulletEnemiesGroup);
    }

    allPropToFalse           = allPropToFalse;
    animPlayerInHisDirection = animPlayerInHisDirection;
    bulletDirection          = bulletDirection;
    bulletEnemyDirection     = bulletEnemyDirection;

    createAnims(){
        // Player
        ['', 'WithoutFeet', 'WithDropsOnFeets'].forEach(variation => {
            this.anims.create({
                key: `player${variation}Start`,
                frames: this.anims.generateFrameNumbers(`link${variation}Walking`, { start: 1, end: 1 }),
                frameRate: 10,
                repeat: -1
            });

            for(let add = 0; add < 49; add+=48){
                let shootingState = add ? 'Shooting' : '';
                this.anims.create({
                    key: `player${variation}WalkToLeft` + shootingState,
                    frames: this.anims.generateFrameNumbers(`link${variation}Walking`, { start: 12 + add, end: 14 + add }),
                    frameRate: 10,
                    repeat: -1
                }); 
                this.anims.create({
                    key: `player${variation}WalkToRight` + shootingState,
                    frames: this.anims.generateFrameNumbers(`link${variation}Walking`, { start: 24 + add, end: 26 + add }),
                    frameRate: 10,
                    repeat: -1
                }); 
                this.anims.create({
                    key: `player${variation}WalkToUp` + shootingState,
                    frames: this.anims.generateFrameNumbers(`link${variation}Walking`, { start: 36 + add, end: 38 + add }),
                    frameRate: 10,
                    repeat: -1
                }); 
                this.anims.create({
                    key: `player${variation}WalkToDown` + shootingState,
                    frames: this.anims.generateFrameNumbers(`link${variation}Walking`, { start: 0 + add, end: 2 + add }),
                    frameRate: 10,
                    repeat: -1
                });
                this.anims.create({
                    key: `player${variation}WalkToDownLeft` + shootingState,
                    frames: this.anims.generateFrameNumbers(`link${variation}Walking`, { start: 3 + add, end: 5 + add }),
                    frameRate: 10,
                    repeat: -1
                });
                this.anims.create({
                    key: `player${variation}WalkToDownRight` + shootingState,
                    frames: this.anims.generateFrameNumbers(`link${variation}Walking`, { start: 15 + add, end: 17 + add }),
                    frameRate: 10,
                    repeat: -1
                });
                this.anims.create({
                    key: `player${variation}WalkToUpLeft` + shootingState,
                    frames: this.anims.generateFrameNumbers(`link${variation}Walking`, { start: 27 + add, end: 29 + add }),
                    frameRate: 10,
                    repeat: -1
                });
                this.anims.create({
                    key: `player${variation}WalkToUpRight` + shootingState,
                    frames: this.anims.generateFrameNumbers(`link${variation}Walking`, { start: 39 + add, end: 41 + add }),
                    frameRate: 10,
                    repeat: -1
                });
                
                this.anims.create({
                    key: `player${variation}RunToLeft` + shootingState,
                    frames: this.anims.generateFrameNumbers(`link${variation}Walking`, { start: 18 + add, end: 20 + add }),
                    frameRate: 10,
                    repeat: -1
                }); 
                this.anims.create({
                    key: `player${variation}RunToRight` + shootingState,
                    frames: this.anims.generateFrameNumbers(`link${variation}Walking`, { start: 21 + add, end: 23 + add }),
                    frameRate: 10,
                    repeat: -1
                }); 
                this.anims.create({
                    key: `player${variation}RunToUp` + shootingState,
                    frames: this.anims.generateFrameNumbers(`link${variation}Walking`, { start: 42 + add, end: 44 + add }),
                    frameRate: 10,
                    repeat: -1
                }); 
                this.anims.create({
                    key: `player${variation}RunToDown` + shootingState,
                    frames: this.anims.generateFrameNumbers(`link${variation}Walking`, { start: 6 + add, end: 8 + add }),
                    frameRate: 10,
                    repeat: -1
                });
                this.anims.create({
                    key: `player${variation}RunToDownLeft` + shootingState,
                    frames: this.anims.generateFrameNumbers(`link${variation}Walking`, { start: 9 + add, end: 11 + add }),
                    frameRate: 10,
                    repeat: -1
                });
                this.anims.create({
                    key: `player${variation}RunToDownRight` + shootingState,
                    frames: this.anims.generateFrameNumbers(`link${variation}Walking`, { start: 21 + add, end: 23 + add }),
                    frameRate: 10,
                    repeat: -1
                });
                this.anims.create({
                    key: `player${variation}RunToUpLeft` + shootingState,
                    frames: this.anims.generateFrameNumbers(`link${variation}Walking`, { start: 33 + add, end: 35 + add }),
                    frameRate: 10,
                    repeat: -1
                });
                this.anims.create({
                    key: `player${variation}RunToUpRight` + shootingState,
                    frames: this.anims.generateFrameNumbers(`link${variation}Walking`, { start: 45 + add, end: 47 + add }),
                    frameRate: 10,
                    repeat: -1
                });
            }
        });

        for(let enemyType in gameParams.enemy.type){
            let suffix = enemyType;
            let add    = gameParams.enemy.type[enemyType].frameStart;

            this.anims.create({
                key: 'enemiesStart' + suffix,
                frames: this.anims.generateFrameNumbers('enemiesWalking', { start: 0 + add, end: 0 + add }),
                frameRate: 10,
                repeat: -1
            });
            this.anims.create({
                key: 'enemies' + suffix + 'WalkToLeft',
                frames: this.anims.generateFrameNumbers('enemiesWalking', { frames: [ 1 + add, 16 + add ] }),
                frameRate: 10,
                repeat: -1
            }); 
            this.anims.create({
                key: 'enemies' + suffix + 'WalkToRight',
                frames: this.anims.generateFrameNumbers('enemiesWalking', { frames: [ 3 + add, 18 + add ] }),
                frameRate: 10,
                repeat: -1
            }); 
            this.anims.create({
                key: 'enemies' + suffix + 'WalkToTop',
                frames: this.anims.generateFrameNumbers('enemiesWalking', { frames: [ 2 + add, 17 + add ] }),
                frameRate: 10,
                repeat: -1
            }); 
            this.anims.create({
                key: 'enemies' + suffix + 'WalkToDown',
                frames: this.anims.generateFrameNumbers('enemiesWalking', { frames: [ 0 + add, 15 + add ] }),
                frameRate: 10,
                repeat: -1
            });
        }

        this.anims.create({
            key: 'fishSwimming',
            frames: this.anims.generateFrameNumbers('fishSwimming', { start: 0, end: 2}),
            frameRate: 10,
            repeat: -1
        });
    }

    createCharacters(){
        this.sounds.playerHurt = this.sound.add('playerHurt');  
        this.sounds.enemyDie   = this.sound.add('enemyDie'); 

        this.createPlayer();
        this.createEnemies();
        this.createFishes();
    }

    createClouds(nbGroups = 5){
        this.clouds  = [];

        for(let n = 0; n < nbGroups; n++){
            let startPos = areneSizes.map.getRandomPoint();
            for(let cloud in gameParams.decor.clouds){
                const cloudInfos = gameParams.decor.clouds[cloud];
                
                for(let i = 0; i < cloudInfos.number; i++){
                    this.clouds.push(this.add.image(startPos.x, startPos.y, cloud).setDepth(4));
                }
            }
            
            Phaser.Utils.Array.Shuffle(this.clouds).forEach((cloud, i) => {
                cloud.x += 4 * rnd_sign() * cloud.width;
                cloud.y += 4 * rnd_sign() * cloud.height;

                cloud.speed = 1 + rnd();
                
                cloud.setScale(5 + rnd() * 5, 1);
            });
        }
    }

    createDecor(){
        this.createClouds();
    }

    createEnemies(){
        this.enemyGroup = this.physics.add.group({ collideWorldBounds: true });

        for(let enemyType in gameParams.enemy.type){
            let enemyOptions = gameParams.enemy.type[enemyType];

            for(let i = 0; i < enemyOptions.number; i++){
                let pos   = this.getRandomPositionOutOfExclusionZones(gameParams.enemy.exclusionZones);
                let enemy = this.physics.add.sprite(pos.x, pos.y).play({ key: 'enemiesStart' + enemyType, repeat: -1 }).setImmovable().setScale(1.72).refreshBody().setInteractive();

                enemy.body.position.x = enemy.x;
                enemy.body.position.y = enemy.y;

                enemy.body.setSize(enemy.width * enemyOptions.sizeCoeff.x, enemy.height * enemyOptions.sizeCoeff.y);
                enemy.body.setOffset(enemy.width * enemyOptions.offsetCoeff.x, enemy.height * enemyOptions.offsetCoeff.y);

                enemy.typeEnemy   = enemyType;
                enemy.typeOptions = enemyOptions;

                enemy.name = "enemy_" + enemyType + "_" + i;

                enemy.on('pointerdown', function (pointer)
                {
                    enemy.talkingTo       = !enemy.talkingTo;
                    this.player.talkingTo = enemy.talkingTo;

                    this.enemyGroup.children.each(function(enemyInGroup) {
                        if(enemyInGroup.name !== enemy.name){ enemyInGroup.talkingTo = false; }
                    }, this);

                }, this);

                this.enemyGroup.add(enemy);
            }
        }
        this.enemyGroup.setDepth(1);
    }

    createFishes(){
        this.fishGroup = this.physics.add.group();

        this.ObjectActionObjectGroup.children.each(objectAction => {
            if(objectAction.data && objectAction.data.list.fishes){
                for(let i = 0; i < objectAction.data.list.fishes; i++){
                    this.fishGroup.add(this.physics.add.sprite(objectAction.x, objectAction.y).play({ key: 'fishSwimming', repeat: -1 }).setImmovable().setScale(1).refreshBody());
                }
            }
        })
    }

    createMap = createMap;

    createPlayer(){
        this.player = this.physics.add.sprite(2784, 3200).play({ key: 'playerStart', repeat: -1 }).setPushable(false).setDrag(222).setScale(0.6).refreshBody();

        this.player.body.setSize(this.player.width/3, this.player.height/Math.SQRT2);
        this.player.body.setOffset(this.player.width * gameParams.offsetCoeff.x, this.player.height * gameParams.offsetCoeff.y);

        this.player.orientTo  = 'down';
        this.player.direction = 'none';
        this.player.nbBurns   = 0;
        this.player.startLife = 100;
        this.player.life      =  this.player.startLife;

        this.player.overlappingObjects = new Set();

        this.player.setDepth(1);
    }

    createSpeechBubble (sprite, width, height, quote, offsetCoeff = {x: 0, y: 1.2}){
        let x = sprite.x - offsetCoeff.x*width;
        let y = sprite.y - offsetCoeff.y*height;

        const bubbleWidth   = width;
        const bubbleHeight  = height;
        const bubblePadding = 10;
        const arrowHeight   = bubbleHeight / 4;

        let bubble = this.add.graphics({ x: x, y: y });
        bubble.offsetCoeff = offsetCoeff;
        bubble.height      = height;
        bubble.width       = width;

        //  Bubble shadow
        bubble.fillStyle(0x222222, 0.5);
        bubble.fillRoundedRect(6, 6, bubbleWidth, bubbleHeight, 16);

        //  Bubble color
        bubble.fillStyle(0xffffff, 1);

        //  Bubble outline line style
        bubble.lineStyle(4, 0x565656, 1);

        //  Bubble shape and outline
        bubble.strokeRoundedRect(0, 0, bubbleWidth, bubbleHeight, 16);
        bubble.fillRoundedRect(0, 0, bubbleWidth, bubbleHeight, 16);

        //  Calculate arrow coordinates
        const point1X = Math.floor(bubbleWidth / 7);
        const point1Y = bubbleHeight;
        const point2X = Math.floor((bubbleWidth / 7) * 2);
        const point2Y = bubbleHeight;
        const point3X = Math.floor(bubbleWidth / 7);
        const point3Y = Math.floor(bubbleHeight + arrowHeight);

        //  Bubble arrow shadow
        bubble.lineStyle(4, 0x222222, 0.5);
        bubble.lineBetween(point2X - 1, point2Y + 6, point3X + 2, point3Y);

        //  Bubble arrow fill
        bubble.fillTriangle(point1X, point1Y, point2X, point2Y, point3X, point3Y);
        bubble.lineStyle(2, 0x565656, 1);
        bubble.lineBetween(point2X, point2Y, point3X, point3Y);
        bubble.lineBetween(point1X, point1Y, point3X, point3Y);

        bubble.content = this.add.text(0, 0, quote, { fontFamily: 'Arial', fontSize: 14, color: '#000000', align: 'center', wordWrap: { width: bubbleWidth - (bubblePadding * 2) } });

        const b = bubble.content.getBounds();

        bubble.content.setPosition(bubble.x + (bubbleWidth / 2) - (b.width / 2), bubble.y + (bubbleHeight / 2) - (b.height / 2));

        sprite.speechBubble = bubble;
    }

    debugTileBelowPointerClick = debugTileBelowPointerClick;
    drawDebug                  = drawDebug;
    fireBullet                 = fireBullet;
    fireCircleBullets          = fireCircleBullets;
    fireBulletEnemy            = fireBulletEnemy;

    forEachLayer(func, ...args){
        gameParams.map[this.runMod].layersObjs.forEach(layer => {
            func(layer, ...args);
        });
    }

    getRandomPositionOutOfExclusionZones(exclusionZones) {
        let position;
        do{
            position = areneSizes.map.getRandomPoint();
        }
        while (isInExclusionZone(position, exclusionZones));
    
        return position;
    }

    getRndDirection = () => ['up', 'right', 'down', 'left'][Math.floor(Math.random() * 4)];

    getTileBelowPlayerByClass(tileClass, layer){
        let tileBelowPlayer = null; 
        [
            {x:  0,  y:   0},
            {x:  0,  y:   15},
            {x:  15, y:   0},
            {x:  15, y:   15},
            {x:  15, y:  -15},
            {x: -15, y:   0},
            {x: -15, y:   15},
            {x: -15, y:  -15},
        ].forEach(dir => {
            let tile = this.map.getTileAtWorldXY(this.player.body.x + dir.x, this.player.body.y + dir.y, false, this.cameras.main, layer);
            if(tile && tile.getTileData() && tile.getTileData().type && tile.getTileData().type === tileClass){ tileBelowPlayer = tile; }
        });

        return tileBelowPlayer;
    }

    globalEvents(){
        this.input.keyboard.on('keydown-A', event => {
            this.enemiesInvDirection = !this.enemiesInvDirection;
        });
        this.input.keyboard.on('keydown-C', event => {
            this.showDebug               = !this.showDebug;
            this.physics.world.drawDebug =  this.showDebug;
            this.enemiesInvDirection     =  this.showDebug;

            this.physics.world.debugGraphic.clear();

            this.forEachLayer(layer => {
                if(layer.currentTilePointerMoveOver){ layer.currentTilePointerMoveOver.tint = TILE_ORIGIN_TINT; }
                if(layer.preventTilePointerMoveOver){ layer.preventTilePointerMoveOver.tint = TILE_ORIGIN_TINT; }
            });

            if(this.allDebugedTiles){ this.allDebugedTiles.forEach(tile => { tile.tint = TILE_ORIGIN_TINT; }); }

            this.drawDebug(gameParams.map[this.runMod], gameParams.map.game.layersObjs);
        });
        this.input.keyboard.on('keydown-D', event => {
            this.isPausedAnimatedTiles = ! this.isPausedAnimatedTiles;
            this.isPausedAnimatedTiles ? this.sys.animatedTiles.setRate(2) : this.sys.animatedTiles.setRate(0.5);
        });
        this.input.keyboard.on('keydown-E', event => {
            this.player.overlappingObjects.forEach(overlappingObject => {
                const platform = overlappingObject.data.list.platform;

                if(platform){
                    this.scene.pause('gameScene');
                    this.scene.setVisible(false, 'gameScene');
                    this.scene.launch('platformScene', {
                        platform          : platform,
                        dataGameScene     : { player  : this.player },
                    });
                }
            });
        });
        this.input.keyboard.on('keydown-F', event => {
            this.fireCircleBullets(12);
        });
        this.input.keyboard.on('keydown-M', event => {
            this.cameras.main.zoom+=0.1;
        });
        this.input.keyboard.on('keydown-O', event => {
            this.cameras.main.zoom = 1;
        });
        this.input.keyboard.on('keydown-L', event => {
            this.cameras.main.zoom-=0.1;
        });
        this.input.keyboard.on('keydown-Q', event => {
            const debugCam = this.cameras.getCamera('debugCam');

            debugCam.setVisible(!debugCam.visible);
        });
        this.input.keyboard.on('keydown-W', event => {
            const tile = this.getTileBelowPlayerByClass('movable', this.layerMovingObject);
            console.log(tile);
            if(tile && tile.getTileData()){
                if(tile.getTileData().type){
                    if(tile.getTileData().type === 'movable'){
                        if(!this.player.tileToMove || !this.player.tileToMove.tile || this.player.tileToMove.tile.index !== tile.index){ this.player.tileToMove = {tileMoving : true, tile: tile}; }
                        else{
                            this.player.tileToMove.tileMoving = !this.player.tileToMove.tileMoving;
                            this.player.tileToMove.tile       = this.player.tileToMove.tileMoving ? tile : null;
                        }
                        if(this.player.tileToMove.tile ){
                            this.colliders.layerMovingObject.betweenPlayerAndLayer.active = false;
                        }
                        else{
                            this.colliders.layerMovingObject.betweenPlayerAndLayer.active = true;
                        }
                    }
                    else{
                        delete this.player.tileToMove;
                        this.colliders.layerMovingObject.betweenPlayerAndLayer.active = true;
                    }
                }
            }
            else{
                delete this.player.tileToMove;
                this.colliders.layerMovingObject.betweenPlayerAndLayer.active = true;
            }
        });
        this.input.keyboard.on('keydown-Z', event => {
            this.enemyGroup.children.entries.length ? this.killAllEnemies() : this.createEnemies(gameParams.enemy.number);
        });

        this.input.on('pointermove', (pointer) => {
            if(this.showDebug){
                this.forEachLayer(layer => {
                    layer.currentTilePointerMoveOver = this.map.getTileAtWorldXY(pointer.worldX, pointer.worldY, false, this.cameras.main, layer);
                    if(layer.currentTilePointerMoveOver){
                        if(layer.preventTilePointerMoveOver && layer.preventTilePointerMoveOver !== layer.currentTilePointerMoveOver){
                            if(!layer.preventTilePointerMoveOver.selected){ layer.preventTilePointerMoveOver.tint = TILE_ORIGIN_TINT; }
                        }
        
                        if(!layer.currentTilePointerMoveOver.selected){ layer.currentTilePointerMoveOver.tint = TILE_MOUSE_OVER_TINT; }
                        layer.preventTilePointerMoveOver      = layer.currentTilePointerMoveOver;
                    }
                });
            }
        });

        this.input.on('pointerdown', (pointer) => {
            const coeff   = gameParams.cameras.debugCam.coeff;
            const camPosX = pointer.downX * coeff.x;
            const camPosY = pointer.downY * coeff.y;
            let camSizX = pointer.downX * (areneSizes.map.width / gameParams.cameras.debugCam.size.x);
            let camSizY = pointer.downY * (areneSizes.map.height / gameParams.cameras.debugCam.size.y);

            camSizX /= 1.83;
            camSizY /= 1.83;

            if(this.cameras.getCamera('debugCam').visible && camSizX <= gameParams.cameras.debugCam.size.x && camSizY <= gameParams.cameras.debugCam.size.y){
                this.player.setPosition(camPosX, camPosY);
            }
        });
    }

    invDirection(direction){ return direction === 'up' ? 'down' : (direction === 'down' ? 'up' : (direction === 'left' ? 'right' : 'left')); }

    isAllPropToFalse = isAllPropToFalse;

    killAllEnemies(){
        this.enemyGroup.clear(true, true);
    }

    makeUserTuches     = makeUserTuches;
    moveBullets        = moveBullets;
    moveBulletsEnemies = moveBulletsEnemies;

    moveClouds(){
        this.clouds.forEach(cloud => {
            cloud.x+=cloud.speed;
            if(cloud.x > areneSizes.map.width + cloud.width){
                cloud.x  = -cloud.width;
                cloud.y += 8 * rnd_sign() * cloud.height;
            }
        });
    }

    moveDecor(){
        this.moveClouds();
    }

    moveEnemies(){
        this.enemyGroup.children.each(function( enemy ) {
            if(!enemy.nbLaps){
                enemy.nbLaps    = 0;
                enemy.direction = null;
                enemy.nbDirections = {
                    directions  : 'none',
                    upOrLeft    : 0,
                    upOrRight   : 0,
                    downOrLeft  : 0,
                    downOrRight : 0,
                };
            }

            enemy.distToPlayer = Phaser.Math.Distance.Between(enemy.x, enemy.y, this.player.x, this.player.y);

            let speedToTurn = enemy.distToPlayer < areneSizes.center.x ? Math.ceil(2 + enemy.typeOptions.speedToTurn * (enemy.distToPlayer / areneSizes.center.x)) : enemy.typeOptions.speedToTurn ;

            let direction = enemy.nbLaps%speedToTurn==0  || !enemy.nbLaps ? this.playerDirectionToEnemy(enemy) : enemy.direction;

            if(this.enemiesInvDirection){ direction = this.invDirection(direction); }

            switch(direction){
                case 'right':
                    enemy.body.setVelocityX(enemy.typeOptions.velocity);
                    enemy.body.setVelocityY(0);
                    enemy.play('enemies' + enemy.typeEnemy + 'WalkToRight', true);
                break;
                case 'left':
                    enemy.body.setVelocityX(-enemy.typeOptions.velocity);
                    enemy.body.setVelocityY(0);
                    enemy.play('enemies' + enemy.typeEnemy + 'WalkToLeft', true);
                break;
                case 'down':
                    enemy.body.setVelocityX(0);
                    enemy.body.setVelocityY(enemy.typeOptions.velocity);
                    enemy.play('enemies' + enemy.typeEnemy + 'WalkToDown', true);
                break;
                case 'up':
                    enemy.body.setVelocityX(0);
                    enemy.body.setVelocityY(-enemy.typeOptions.velocity);
                    enemy.play('enemies' + enemy.typeEnemy + 'WalkToTop', true);
                break;
            }

            enemy.direction = direction;

            if(enemy.nbLaps%enemy.typeOptions.bullet.frameRate==0 && Phaser.Math.Distance.Between(this.player.x, this.player.y, enemy.x, enemy.y) < enemy.typeOptions.distToShoot){
                this.fireBulletEnemy(enemy);
            }

            enemy.nbLaps++;
          }, this);
    }

    moveFishes(){
        this.fishGroup.children.each(function( fish ) {
            let speed       = {x: 0, y: 0};
            const baseSpeed = 50;

            if(!fish.nbMoves){
                fish.nbMoves   = 1;
                fish.proba     = (rnd() + 0.5) / 2;
                speed          = {x: baseSpeed * rnd_sign(), y: baseSpeed * rnd_sign()};
            }
            else{
                if(fish.nbMoves%(2*NB_FPS)==0) { fish.proba = 0.75 - fish.proba; }
                if(fish.nbMoves%(8*NB_FPS)==0) { fish.proba = (rnd() + 0.5) / 2; }
                if(fish.nbMoves%(16*NB_FPS)==0){ fish.proba = 0.5; }
                
                speed = direction(fish.rotation + (rnd_sign(fish.proba)/10), baseSpeed);
            } 

            fish.body.setVelocityX(speed.x);
            fish.body.setVelocityY(speed.y);

            let angle = Math.atan2(fish.body.velocity.y, fish.body.velocity.x);

            fish.rotation = angle;

            fish.nbMoves++;
        });
    }

    playerOverlappingObjects(){
        this.player.overlappingObjects.forEach(object => {
        if (!Phaser.Geom.Intersects.RectangleToRectangle(this.player.getBounds(), object.getBounds())) {
            this.player.overlappingObjects.delete(object);
        }
    });
    }

    movePlayer(){
        this.playerOverlappingObjects();

        const keys = this.keys;

        let step = {supOne: 0.1, infOne: 0.01};

        if(keys.plus.isDown === true) {
            this.scaleEnemiesVelocity(1.05);
        }
        if(keys.minus.isDown === true) {
            this.scaleEnemiesVelocity(0.95);
        }
        if(keys.six.isDown === true) {
            gameParams.offsetCoeff.y >= 1 ? gameParams.offsetCoeff.y+=step.supOne : gameParams.offsetCoeff.y+=step.infOne;
        }
        if(keys.nine.isDown === true) {
            gameParams.offsetCoeff.y > 1 ? gameParams.offsetCoeff.y-=step.supOne : gameParams.offsetCoeff.y-=step.infOne;
        }
        if(keys.four.isDown === true && !this.arrowKeys.four) {
            this.arrowKeys.four = true;
            requestOpenai("Qui est la reine d'angleterre ?", "res");
        }
        else if(keys.four.isUp === true) {
            this.arrowKeys.four = false;
        }

        //************************** Déplacement du player **************************//
        if(this.player.direction !== 'none'){ this.player.orientTo = this.player.direction; }
        this.player.direction  = 'none';
        this.player.isShooting = false;

        this.allPropToFalse(this.arrowKeys);
        if(this.player.overlapTileType !== 'sliding'){ this.player.setVelocity(0); }

        let velocity = !this.cursors.shift.isDown ? gameParams.player.velocity : 2 * gameParams.player.velocity;

        if(this.cursors.left.isDown){
            this.arrowKeys.left = true;

            this.player.body.setVelocityX(-velocity);
        }
        if(this.cursors.right.isDown === true){
            this.arrowKeys.right = true;

            this.player.body.setVelocityX(velocity);
        }
        if(this.cursors.up.isDown === true){
            this.arrowKeys.up = true;

            this.player.body.setVelocityY(-velocity);
        }
        if(this.cursors.down.isDown === true){
            this.arrowKeys.down = true;

            this.player.body.setVelocityY(velocity);
        }

        this.player.direction = this.playerDirection();

        if(!this.isAllPropToFalse(this.arrowKeys) && this.player.tileToMove && this.player.tileToMove.tileMoving && this.player.tileToMove.tile){
            this.moveTile(this.player.tileToMove.tile, this.player.direction, this.layerMovingObject);
        }

        if(Phaser.Input.Keyboard.JustDown(this.spaceKey)){
            this.player.isShooting = true;

            this.fireBullet(this.bulletDirection());
        }

        this.animPlayerInHisDirection();
        //**************************************************************************//
    }

    moveTile(tile, direction, layer){
        let collides = tile.collides;

        this.map.removeTileAt(tile.x, tile.y, true, true, layer);

        let newPosInTileX = this.map.worldToTileX(this.player.x);
        let newPosInTileY = this.map.worldToTileY(this.player.y);

        let newDir = this.strDirToObjDir(direction);

        newPosInTileX += newDir.x;
        newPosInTileY += newDir.y;

        this.player.tileToMove.tile = this.map.putTileAt(tile, newPosInTileX, newPosInTileY, true, layer);
        this.player.tileToMove.tile.setCollision(collides);
    }

    pauseOrResume(){
        if(Phaser.Input.Keyboard.JustDown(this.pauseKey)){
            this.isOnPause = !this.isOnPause;
        }
        return this.isOnPause;
    }

    playerDirection = playerDirection;

    playerDirectionToEnemy(enemy){
        let lim = 2;

        let angleBetweenEnemyAndPlayer = Phaser.Math.Angle.Between(this.player.x, this.player.y, enemy.x, enemy.y);

        const directions = enemy.nbDirections.directions;
        
        if(angleBetweenEnemyAndPlayer >=0 && angleBetweenEnemyAndPlayer < HALF_PI)   {
            if(directions === 'none' || directions === 'upOrLeft'){ enemy.nbDirections.upOrLeft++; }
            else{ razDirs(); }
            enemy.nbDirections.directions = 'upOrLeft';

            if(enemy.nbDirections.upOrLeft < lim){ return either('up', 'left'); }

            razDirs();
            return either('down', 'right');
        }
        if(angleBetweenEnemyAndPlayer >=HALF_PI && angleBetweenEnemyAndPlayer < PI)  {
            if(directions === 'none' || directions === 'upOrRight'){ enemy.nbDirections.upOrRight++; }
            else{ razDirs(); }
            enemy.nbDirections.directions = 'upOrRight';

            if(enemy.nbDirections.upOrRight < lim){ return either('up', 'right'); }

            razDirs();
            return either('down', 'left');
        }
        if(angleBetweenEnemyAndPlayer >=-HALF_PI && angleBetweenEnemyAndPlayer < 0)  {
            if(directions === 'none' || directions === 'downOrLeft'){ enemy.nbDirections.downOrLeft++; }
            else{ razDirs(); }
            enemy.nbDirections.directions = 'downOrLeft';

            if(enemy.nbDirections.downOrLeft < lim){ return either('down', 'left'); }

            razDirs();
            return either('up', 'right'); 
        }
        if(angleBetweenEnemyAndPlayer >=-PI && angleBetweenEnemyAndPlayer < -HALF_PI){
            if(directions === 'none' || directions === 'downOrRight'){ enemy.nbDirections.downOrRight++; }
            else{ razDirs(); }
            enemy.nbDirections.directions = 'downOrRight';

            if(enemy.nbDirections.downOrRight < lim){ return either('down', 'right'); }

            razDirs();
            return either('up', 'left');
        }

        function razDirs(){ for(let prop in enemy.nbDirections){ enemy.nbDirections[prop] = 0; } }
    }

    scaleEnemiesVelocity(scale){
        for(let enemyType in gameParams.enemy.type){
            gameParams.enemy.type[enemyType].velocity *= scale;
            gameParams.enemy.type[enemyType].bullet.velocity *= scale;
        }
    }

    scalePlayerSize(scale){
        this.player.setScale(this.player.scale * scale).refreshBody();
    }

    setCamera(){
        this.cameras.main.setBounds(0, 0, areneSizes.map.width, areneSizes.map.height);
        this.cameras.main.setZoom(1);
        this.cameras.main.centerOn(0, 0);
        this.cameras.main.startFollow(this.player, true, 0.5, 0.5);
    }

    setMapTilesSize = setMapTilesSize;

    setTileProperty(mapLayer, tileIndex, property, value) {
        var d = mapLayer.layer.data;
        
        for (var i = 0; i < d.length; i++) {
            for (var j = 0; j < d[i].length; j++) {
                let tile = d[i][j];
                if(tile.index === tileIndex){
                    if(tile['set' + property.firstLetterInMaj]){ tile[set + property.capitalize()](value); }
                    else{ tile[property] = value; }

                    break;
                }
            }
        }   
    }

    setTileSidesWithNoCollision(mapLayer, ...sidesNoCollide) {
        var d = mapLayer.layer.data;
        
        for (var i = 0; i < d.length; i++) {
            for (var j = 0; j < d[i].length; j++) {
                var t = d[i][j];
                sidesNoCollide.forEach(sideNoCollide => {
                    if (t.properties[sideNoCollide] === false) {
                        t[sideNoCollide] = false;
                    }
                });
            }
        }
    }

    showBubbles(){
        this.showTextBubble(this.player, "", true, 0, 0);
        this.enemyGroup.children.each(function(enemy) {
            this.showTextBubble(enemy, '“You talking to me ?”', false, 220, 220);
        }, this);
    }

    showDebugTilesInfos = showDebugTilesInfos;
    showHealthBar       = showHealthBar;

    showTextBubble(sprite, text = '“You talking to me ?”', inputText, width, height){
        if(sprite.talkingTo){
            if(!sprite.speechBubble){
                this.createSpeechBubble(sprite, width, height, text);
                if(inputText){
                    sprite.inputText = this.add.dom(sprite.x, sprite.y).createFromCache('domInput');
                    sprite.inputText.addListener('keyup');
                    sprite.inputText.on('keyup', function(e){
                        if(e.key === 'Enter'){
                            requestOpenai(e.target.value, "res");
                        }
                    });
                }
            }
            else{
                let bubblePosY = sprite.y - sprite.speechBubble.offsetCoeff.y*sprite.speechBubble.height;

                sprite.speechBubble.x = sprite.x;
                sprite.speechBubble.y = bubblePosY;

                sprite.speechBubble.content.x = sprite.x + 5;
                sprite.speechBubble.content.y = bubblePosY;

                if(inputText && sprite.inputText){
                    sprite.inputText.setPosition(sprite.x, sprite.y);
                    sprite.inputText.width  = '500px';
                    sprite.inputText.height = '40px';

                    sprite.inputText.node.childNodes[0].width = '500px';
                    sprite.inputText.node.childNodes[0].height = '500px';
                }
                else if(openaiResponse.res && openaiResponse.res.text){
                    sprite.speechBubble.content.destroy();
                    sprite.speechBubble.destroy();

                    delete sprite.speechBubble.content;
                    delete sprite.speechBubble;

                    let response = openaiResponse.res.text.match(/\./g).length > 1 ? openaiResponse.res.text.substr(0, openaiResponse.res.text.indexOf('.') + 1) : openaiResponse.res.text;

                    this.createSpeechBubble(sprite, 220, 220, response);
                }
            }
        }
        else if(sprite.speechBubble !== undefined){
            sprite.speechBubble.content.destroy();
            sprite.speechBubble.destroy();

            delete sprite.speechBubble.content;
            delete sprite.speechBubble;

            if(!this.enemyGroup.children.entries.some(enemy => enemy.talkingTo) && this.player.inputText){
                this.player.inputText.destroy();
                delete this.player.inputText;
            }
        }
    }

    showTextTestOnUpdate(){
        if(this.testText){
            this.testText.x.destroy();
            this.testText.y.destroy();
        }
        else{
            this.testText = {}; 
        }

        this.testText.x = this.add.text(30, 30, "offsetCoeff X : " + gameParams.offsetCoeff.x, { fontFamily: 'Georgia, "Goudy Bookletter 1911", Times, serif' });
        this.testText.y = this.add.text(30, 60, "offsetCoeff Y : " + gameParams.offsetCoeff.y, { fontFamily: 'Georgia, "Goudy Bookletter 1911", Times, serif' });
    }
    
    strDirToObjDir = strDirToObjDir;    

    tintTileByWorldPos(pos){
        this.forEachLayer(layer => {
            layer.currentTileShowByFunction = this.map.getTileAtWorldXY(pos.x, pos.y, false, this.cameras.main, layer);
            if(layer.currentTileShowByFunction){
                if(layer.preventTileShowByFunction && layer.preventTileShowByFunction !== layer.currentTileShowByFunction){
                    if(!layer.preventTileShowByFunction.selected){ layer.preventTileShowByFunction.tint = TILE_ORIGIN_TINT; }
                }

                if(!layer.currentTileShowByFunction.selected){ layer.currentTileShowByFunction.tint = TILE_SELECTED_BY_FUNCTION_TINT; }
                layer.preventTileShowByFunction = layer.currentTileShowByFunction;
            }
        });
    }
    
    varyPlayerLife = varyPlayerLife;

    zeTiles(timeToReturn = 0, reverse = false){
        let tiles = [];
        for(let i = 0; i < 2000; i++){
            gameParams.map.game.layersObjs.forEach(layer => {
                let tile = this.map.findByIndex(i, timeToReturn, reverse, layer);
                if(tile){ tiles.push(tile); }
            });
            gameParams.zeTiles = tiles;
        } 
    }

}

export default GameScene;