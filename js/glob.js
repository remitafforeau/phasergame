const areneSizes  = {
    width  : 1700,
    height : 1040,
    map    : {
        width  : 6400,
        height : 6400,
    },
    platform : {
        width  : 1600,
        height : 3200,
    }
};
areneSizes.center = { x: areneSizes.width / 2, y: areneSizes.height / 2 };

areneSizes.getRandomPoint = (coeff = 1) => {
    return {
        x: Math.round(Math.random() * areneSizes.width  * coeff, 0),
        y: Math.round(Math.random() * areneSizes.height * coeff, 0),
    };
}
areneSizes.map.getRandomPoint = (coeff = 1) => {
    return {
        x: Math.round(Math.random() * areneSizes.map.width  * coeff, 0),
        y: Math.round(Math.random() * areneSizes.map.height * coeff, 0),
    };
}

let gameParams = {
    map: {
        game:{
            filePath        : "map/bigMapTest3.json",
            key             : "gamemap",
            isAnimatedTiles : true,
            tileset  : {
                filePath : "map/landOthers.png",
                key      : "tiles_img",
            },
            layers : [
                {
                    name    : 'Floor',
                    collide : false,
                },
                {
                    name    : 'Background',
                    collide : false,
                    overlap : true,
                },
                {
                    name    : 'StaticObject-back',
                    collide : false,
                },
                {
                    name    : 'StaticObject-front',
                    collide : false,
                    depth   : 2,
                },
                {
                    name        : 'MovingObject',
                    collide     : true,
                    autoCollide : true,
                    collideWithOthersObjectLayers : [
                        'TerrainCollision',
                        'ObjectCollision',
                    ],
                },
                {
                    name    : 'StaticObject-frontAll',
                    collide : false,
                    depth   : 3,
                },
                {
                    name                : 'TerrainCollision',
                    type                : 'object',
                    collide             : true,
                    groupsWithNoCollide : ['bulletGroup', 'bulletEnemiesGroup'],
                },
                {
                    name        : 'ObjectCollision',
                    type        : 'object',
                    collide     : true,
                },
                {
                    name        : 'ObjectAction',
                    type        : 'object',
                    collide     : false,
                    overlap     : true,
                },
            ],
            platformScenes : {
                centralHouse : {
                    filePath        : "map/platform.json",
                    key             : "platformMap",
                    isAnimatedTiles : true,
                    tileset : {
                        filePath : "map/platform.png",
                        key      : "platformTiles",
                    },
                    player : {
                        startPos : {x: 128, y: 3072},
                    },
                    layers : [
                        {
                            name    : 'Background',
                            collide : false,
                            overlap : false,
                        },
                        {
                            name    : 'StaticObject-back',
                            collide : false,
                            overlap : false,
                        },
                        {
                            name    : 'StaticObject-front',
                            collide : true,
                            overlap : false,
                        },
                        {
                            name        : 'ObjectCollision',
                            type        : 'object',
                            collide     : true,
                        },
                        {
                            name        : 'ObjectAction',
                            type        : 'object',
                            collide     : false,
                            overlap     : true,
                        },
                    ],
                },
                centralStatut : {
                    filePath        : "map/platform2.json",
                    key             : "platformMap2",
                    isAnimatedTiles : true,
                    tileset : {
                        filePath : "map/platform.png",
                        key      : "platformTiles2",
                    },
                    player : {
                        startPos : {x: 128, y: 3072},
                    },
                    layers : [
                        {
                            name    : 'Background',
                            collide : false,
                            overlap : false,
                        },
                        {
                            name    : 'StaticObject-back',
                            collide : false,
                            overlap : false,
                        },
                        {
                            name    : 'StaticObject-front',
                            collide : true,
                            overlap : false,
                        },
                        {
                            name        : 'ObjectCollision',
                            type        : 'object',
                            collide     : true,
                        },
                        {
                            name        : 'ObjectAction',
                            type        : 'object',
                            collide     : false,
                            overlap     : true,
                        },
                    ],
                },
            },
        },
        test: {
            filePath        : "map/bigMapTest.json",
            isAnimatedTiles : true,
            tileset  : {
                filePath : "map/landOthers.png",
            },
            layers : [
                {
                    name:   'Background-Rock',
                    collide : false
                },
                {
                    name    : 'Background-FLoors',
                    collide : true
                },
                {
                    name    : 'Background-Rocks',
                    collide : false
                },
                {
                    name    : 'Background-ObjectBloc',
                    collide : true, 
                    depth   : 2,
                },
                {
                    name    : 'Background-ObjectNoBloc',
                    collide : false,
                },
            ],     
        },
    },
    cameras: {
        debugCam : {
            size : {
                x : 1500,
                y : 1500,
            },
        },
    },
    player: {
        velocity      : 200,
        bullet    : {
            sprite   : 'bullet',
            velocity : 300,
            radius   : 6.5,
        },
        healthBar : {
            pos: {
                x : 30,
                y : 30,
            },
            sizes: {
                width  : 300,
                height : 16,
            },
            border : {
                thickness : 2,
            },
            colors : {
                border : 0x000000,
                bg     : {
                    noHealth : 0xffffff,
                },
                life : {
                    inHealth : 0x00ff00,
                    inDanger : 0xff0000,
                }
            },
            lifeInDangerLimit : 0.3,
        }
    },
    enemy: {
        number         : 100,
        exclusionZones : [
            {x: 1344, y: 2656, width: 3296, height: 1152},
        ],
        type : {
            insect : {
                number      : 25,
                frameStart  : 0,
                velocity    : 200,
                distToShoot : 666,
                power       : 10,
                speedToTurn : 60,
                bullet      : {
                    sprite    : 'bulletEnemyInsectRed',
                    velocity  : 300,
                    frameRate : 240,
                    radius    : 6.5,
                },
                offsetCoeff: {
                    x: 0,
                    y: -1/32,
                },
                sizeCoeff: {
                    x: Math.SQRT1_2,
                    y: Math.SQRT1_2,
                },
            },
            insectBlue : {
                number      : 35,
                frameStart  : 4,
                velocity    : 100,
                distToShoot : 666,
                power       : 5,
                speedToTurn : 120,
                bullet      : {
                    sprite    : 'bulletEnemyInsectBlue',
                    velocity  : 150,
                    frameRate : 480,
                    radius    : 14,
                },
                offsetCoeff: {
                    x: 1/8,
                    y: -1/16,
                },
                sizeCoeff: {
                    x: Math.SQRT1_2,
                    y: Math.SQRT1_2,
                },
            },
            man : {
                platform    : true,
                number      : 10,
                frameStart  : 30,
                velocity    : 400,
                distToShoot : 666,
                power       : 20,
                speedToTurn : 30,
                bullet      : {
                    sprite    : 'bulletEnemyMan',
                    velocity  : 600,
                    frameRate : 120,
                },
                offsetCoeff: {
                    x: 1/16,
                    y: 1/16,
                },
                sizeCoeff: {
                    x: 0.5,
                    y: 0.7,
                },
            },
            manBlue : {
                platform    : true,
                number      : 10,
                frameStart  : 34,
                velocity    : 200,
                distToShoot : 666,
                power       : 20,
                speedToTurn : 60,
                bullet      : {
                    sprite    : 'bulletEnemyManBlue',
                    velocity  : 300,
                    frameRate : 240,
                },
                offsetCoeff: {
                    x: 1/6,
                    y: 1/16,
                },
                sizeCoeff: {
                    x: 0.55,
                    y: 0.7,
                },
            },
            manStrongBlue : {
                platform    : true,
                number      : 20,
                frameStart  : 64,
                velocity    : 200,
                distToShoot : 666,
                power       : 50,
                speedToTurn : 60,
                bullet      : {
                    sprite    : 'bulletEnemyManStrongBlue',
                    velocity  : 600,
                    frameRate : 40,
                    radius    : 8,
                },
                offsetCoeff: {
                    x: 1/6,
                    y: 1/16,
                },
                sizeCoeff: {
                    x: 0.55,
                    y: 0.7,
                },
            },
        },
    },
    decor: {
        clouds: {
            cloud1: {
                number: 5,
            },
        },
    },
    offsetCoeff: {
        x: 1/3,
        y: 0.25,
    }
};

gameParams.cameras.debugCam.coeff = {
    x : 2.35 * areneSizes.map.width / gameParams.cameras.debugCam.size.x,
    y : 2.35 * areneSizes.map.height / gameParams.cameras.debugCam.size.y,
};

const dirInv  = {left: 'right', right: 'left', up: 'down', down : 'up'};
let zeScene   = {};

const PI      = Math.PI;
const PI2     = 2 * PI;
const HALF_PI = PI/2;

const either = (event1, event2) => Math.random() < 0.5 ? event1 : event2;

const TILE_ORIGIN_TINT               = 16777215;
const TILE_MOUSE_OVER_TINT           = 9539985;
const TILE_SELECTED_TINT             = 7829367;
const TILE_SELECTED_BY_FUNCTION_TINT = 15777215;

const NB_FPS = 60;

const rnd = Math.random;
const cos = Math.cos;
const sin = Math.sin;
const tan = Math.tan;


Object.defineProperty(String.prototype, 'capitalize', {
    value: function() {
      return this.charAt(0).toUpperCase() + this.slice(1);
    },
    enumerable: false
});