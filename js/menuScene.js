/* global Phaser */

class MenuScene extends Phaser.Scene {
    constructor(){
        super({ key: 'menuScene' });

        this.menuSceneBackgroundImage = null;
        this.startButton              = null;
        this.testButton               = null;
    }

    init(data){
        this.cameras.main.setBackgroundColor('#ffffff');
    }

    preload(){
        console.log("Menu Scene");
        this.load.image('menuSceneBackground', 'assets/aliens_screen_image2.jpg');
        this.load.image('startButton', 'assets/start.png');
        this.load.image('testButton', 'assets/testButton.png');
    }

    create(data){
        this.menuSceneBackgroundImage   = this.add.sprite(0, 0, 'menuSceneBackground');
        this.menuSceneBackgroundImage.x = areneSizes.center.x;
        this.menuSceneBackgroundImage.y = areneSizes.center.y;

        this.startButton = this.add.sprite(areneSizes.center.x - 200, areneSizes.center.y + 100, 'startButton');
        this.startButton.setInteractive({ useHandCursor: true });
        this.startButton.on('pointerdown', () => this.clickButton('game'));

        this.testButton = this.add.sprite(areneSizes.center.x + 200, areneSizes.center.y + 100, 'testButton');
        this.testButton.setInteractive({ useHandCursor: true });
        this.testButton.on('pointerdown', () => this.clickButton('test'));
    }

    update(time, delta){
        
    }

    clickButton(mod){
        this.scene.start('gameScene', {mod: mod});
    }
}

export default MenuScene;