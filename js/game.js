/* global Phaser */

import SplashScene   from './splashScene.js';
import TitleScene    from './titleScene.js';
import MenuScene     from './menuScene.js';
import GameScene     from './gameScene.js';
import PlatformScene from './platformScene.js';

const splashScene   = new SplashScene();
const titleScene    = new TitleScene();
const menuScene     = new MenuScene();
const gameScene     = new GameScene();
const platformScene = new PlatformScene();

const config = {
    width  : areneSizes.width,
    height : areneSizes.height,
    parent : 'gameContainer',  
    dom    : {
        createContainer: true
    },
    physics : {
        default : 'arcade',
        arcade  :{
            debug: false,
        },
    },
    backgroundColor : 0xffffff,
    scale: {
        mode        : Phaser.Scale.FIT,
        autoCenter  : Phaser.Scale.CENTER_BOTH,
    },
    type : Phaser.AUTO,
};

const game = new Phaser.Game(config);

game.scene.add('splashScene', splashScene);
game.scene.add('titleScene', titleScene);
game.scene.add('menuScene', menuScene);
game.scene.add('gameScene', gameScene);
game.scene.add('platformScene', platformScene);

game.scene.start('splashScene');