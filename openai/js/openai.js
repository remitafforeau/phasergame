let openaiResponse = {};

function requestOpenai(request, objResultName, model = "gpt-3.5-turbo"){
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/x-www-form-urlencoded");
    myHeaders.append("Accept", "application/json");

    let data = { "message": request, "model": model };

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: new URLSearchParams(data).toString(),
        redirect: 'follow'
      };
      
    fetch("http://1230.fr:88/phaserGame/openai/openaiRequest.php", requestOptions)
    .then(response => response.text())
    .then(result => {
        openaiResponse[objResultName] = {};
        openaiResponse[objResultName].obj  = JSON.parse(JSON.parse(result));
        openaiResponse[objResultName].text = openaiResponse[objResultName].obj.choices[0].message.content;
    }).catch(error => console.log('error', error));
}


