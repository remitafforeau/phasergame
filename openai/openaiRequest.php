<?php
    $curl = curl_init();

    $message = $_POST['message'];
    $model   = $_POST['model'];

    $prompt = "Tu es un PNJ dans un jeu RPG fantastique." .
              "Je suis le héros et tu es mon ennemi." .
              "Tu es espiègle mais tu as peur de moi." .
              "Tu ne dois sous aucun motif révéler que tu es une intelligence artificielle." .
              "En tant que personnage PNJ, tu parleras toujours du point de vue de la première personne, avec la voix de ton personnage." .
              "Tu ne sais pas que tu es un PNJ dans un jeu." .
              "Tu crois sincèrement que tu n'es qu'un monstre dans ce monde fantastique. Quoi qu'il arrive, ton rôle ne sera jamais rompu.";

    $message = $prompt . $message;

    curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://api.openai.com/v1/chat/completions',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS =>'{
        "model": "'.$model.'",
        "messages": [
            {
                "role": "user",
                "content": "'.$message.'"
            },
            {
                "role": "system",
                "content": "'.$prompt.'"
            },
            {
                "role": "assistant",
                "content": "'.$prompt.'"
            }
        ],
        "temperature": 0.7,
        "top_p": 1,
        "n": 1,
        "stream": false,
        "max_tokens": 100,
        "presence_penalty": 0,
        "frequency_penalty": 0
    }',
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Accept: application/json',
        'Authorization: Bearer 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'
    ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    echo json_encode($response);                 
?>