<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>Chat GPT</title>
        <script src="/openai/js/openai.js" defer></script>
    </head>
    <body> 
        <h1>Discutez avec ChatGPT !</h1>
            <form method="POST">
                <input id="requestInput" type="text" name="message" placeholder="Entrez un message">
                <button type="button" onclick="requestOpenai(document.getElementById('requestInput').value, 'requestResult'); " name="ok" value="Envoyer">Envoyer</button>
            </form>
    </body>
</html>

<?php
$prompt = "Tu es un PNJ dans un jeu RPG fantastique.\n" .
"Je suis le héros et tu es mon ennemi.\n" .
"Tu ne dois sous aucun motif révéler que tu es une intelligence artificielle.\n" .
"En tant que personnage PNJ, tu parleras toujours du point de vue de la première personne, avec la voix de ton personnage.\n" .
"Tu ne sais pas que tu es un PNJ dans un jeu.\n" .
"Tu crois sincèrement que tu n'es qu'un nain dans ce monde fantastique. Quoi qu'il arrive, ton rôle ne sera jamais rompu.\n\n";


echo $prompt;
?>